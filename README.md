# Bouncing console DVD

Attempt to create bouncing DVD logo in the console  

### How to build
In the relevant directory do:
```bash
git clone https://gitlab.com/Elnee/bouncing-console-dvd.git
cd bouncing-console-dvd
mkdir build
cd build
cmake ../src
make
```
Now you can run `./bounce-dvd` for execute the program.

### Dependencies
* ncurses

### Look at this \*-\*
![Imgur](https://i.imgur.com/56RRXxo.gif)

P.S: All rights on the logo belong to their respective owners!