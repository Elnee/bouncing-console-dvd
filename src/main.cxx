#include <iostream>
#include <ncurses.h>
#include <unistd.h>
#include <ctime>
#include <cstdlib>

#define DELAY 50000

// Color pairs
#define WHITE   0
#define RED     1
#define GREEN   2
#define YELLOW  3
#define BLUE    4
#define MAGENTA 5
#define CYAN    6

using namespace std;

int next_color(int current_color);

int main(int argc, char* argv[])
{
  initscr(); // Initialize the window
  noecho(); // Don't echo any keypresses
  curs_set(FALSE); // Don't display a cursor

  // DVD Logo in ASCII
  // TODO: Improve the logo
  const int logo_width = 21; // Actually 20 but +1 null terminated symbol (\0)
  const int logo_height = 9; // Amount of strings to represent logo
  char ascii_logo_strings[logo_height][logo_width] = {
    "  #######    #####  ",
    "    .####.  ##   ##t",
    " ##   #### ## #   ##",
    " ##  ## #### ##  ##W",
    " #####:  ##  #####D ",
    "         #          ",
    "   D###########L    ",
    "##### #   ;#G;##### ",
    "    f#########j     "
  };

  // ~~~~ Algorithm ~~~~
  int max_y, max_x;

  /* Variable to keep movement direction
   * 0 - TOP LEFT * 1 - TOP RIGHT * 2 - BOTTOM LEFT * 3 - BOTTOM RIGHT */
  int move_direction; 

  // Structure to keep coordinates
  struct {
    int y;
    int x;
  } coords;

  // Pick random coordinates to place the logo at initial position
  getmaxyx(stdscr, max_y, max_x);
  srand(time(NULL));

  coords.y = rand() % (max_y - logo_height);
  coords.x = rand() % (max_x - logo_width);

  // Pick random movement direction
  move_direction = rand() % 4;

  // Setup colorful output and color pairs
  start_color();
  init_pair(WHITE,   COLOR_WHITE,   COLOR_BLACK);
  init_pair(RED,     COLOR_RED,     COLOR_BLACK);
  init_pair(GREEN,   COLOR_GREEN,   COLOR_BLACK);
  init_pair(YELLOW,  COLOR_YELLOW,  COLOR_BLACK);
  init_pair(BLUE,    COLOR_BLUE,    COLOR_BLACK);
  init_pair(MAGENTA, COLOR_MAGENTA, COLOR_BLACK);
  init_pair(CYAN,    COLOR_CYAN,    COLOR_BLACK);

  int current_color = WHITE;

  while(true) {
    getmaxyx(stdscr, max_y, max_x); // Get maximum x and y coordinates
    clear(); // Clear what was previous printed

    attron(COLOR_PAIR(current_color)); // Apply specific color

    // Print the logo
    for (int i = 0; i < logo_height; ++i) {
      mvprintw(coords.y + i, coords.x, ascii_logo_strings[i]);
    }

    // Move the logo in chosen direction
  
    // Top left direction
    if (move_direction == 0) {
      coords.x -= 1; coords.y -= 1;
    }

    // Top right direction
    if (move_direction == 1) {
      coords.x += 1; coords.y -= 1;
    }

    // Bottom left direction
    if (move_direction == 2) {
      coords.x -= 1; coords.y += 1;
    }

    // Bottom right direction
    if (move_direction == 3) {
      coords.x += 1; coords.y += 1;
    }

    // Collision detection
    
    // Top collision
    if (coords.y == 0) {
      if (move_direction == 0) move_direction = 2;
      if (move_direction == 1) move_direction = 3;
    }

    // Right wall collision
    if (coords.x + logo_width - 1 == max_x) {
      if (move_direction == 3) move_direction = 2;
      if (move_direction == 1) move_direction = 0;
    }

    // Bottom collision
    if (coords.y + logo_height == max_y) {
      if (move_direction == 2) move_direction = 0;
      if (move_direction == 3) move_direction = 1;
    }

    // Left wall collision
    if (coords.x == 0) {
      if (move_direction == 0) move_direction = 1;
      if (move_direction == 2) move_direction = 3;
    }

    // Change color after bounce
    if (coords.y == 0 || coords.x + logo_width - 1 == max_x || coords.y + logo_height == max_y || coords.x == 0) current_color = next_color(current_color);

    refresh(); // Refresh current window to see the chars
    usleep(DELAY); // Pause between outputs on the screen
  }

  endwin(); // Close window
  return 0;
}

int next_color(int current_color)
{
  if (current_color < 6) return ++current_color;
  return 0;
}
